package es.loycus.quiz.controller

import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class QuizControllerTest : QuizTestWithFongoAndMockMvc() {

  @Test
  fun findTest() {
    logger.info("Begin findTest")

    mvc.perform(MockMvcRequestBuilders
      .get("/quiz/${TEST_QUIZ.id}"))
      .andExpect(MockMvcResultMatchers.status().isOk)
//      .andExpect(MockMvcResultMatchers.content().json(readFile(REQUEST_FILE)))
  }

  @Test
  fun saveTest() {
    logger.info("Begin saveTest")

    mvc.perform(MockMvcRequestBuilders
      .post("/quiz")
      .contentType(MediaType.APPLICATION_JSON)
      .content(readFile(REQUEST_FILE)))
      .andExpect(MockMvcResultMatchers.status().isOk)
  }

  @Test
  fun updateTest() {
    logger.info("Begin updateTest")

    mvc.perform(MockMvcRequestBuilders
      .put("/quiz/${TEST_QUIZ.id}")
      .contentType(MediaType.APPLICATION_JSON)
      .content(readFile(REQUEST_FILE)))
      .andExpect(MockMvcResultMatchers.status().isOk)
  }

  @Test
  fun deleteTest() {
    logger.info("Begin deleteTest")

    mvc.perform(MockMvcRequestBuilders
      .delete("/quiz/${TEST_QUIZ.id}"))
      .andExpect(MockMvcResultMatchers.status().isOk)
  }

  companion object {
    val logger: Logger = LoggerFactory.getLogger(QuizControllerTest::class.java)
  }

}