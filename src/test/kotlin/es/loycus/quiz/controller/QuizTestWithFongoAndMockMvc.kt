package es.loycus.quiz.controller

import es.loycus.quiz.QuizTestWithFongo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.test.web.servlet.MockMvc

@AutoConfigureMockMvc
abstract class QuizTestWithFongoAndMockMvc(initializeTestData: Boolean = true) : QuizTestWithFongo(initializeTestData) {
  @Autowired
  lateinit var mvc : MockMvc
}