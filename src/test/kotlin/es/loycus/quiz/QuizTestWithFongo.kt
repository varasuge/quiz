package es.loycus.quiz

import com.github.fakemongo.junit.FongoRule
import es.loycus.quiz.model.Option
import es.loycus.quiz.model.Question
import es.loycus.quiz.model.Quiz
import es.loycus.quiz.model.Topic
import es.loycus.quiz.repository.QuizRepository
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
abstract class QuizTestWithFongo(val initializeTestData: Boolean = true) {

  @get:Rule
  val fongoRule = FongoRule()

  @Autowired
  lateinit var quizRepository: QuizRepository

  @Before
  fun setupTestDatabase() {
    if (initializeTestData) {
      quizRepository.save(TEST_QUIZ)
    }
  }

  companion object {
    const val REQUEST_FILE = "request.json"

    const val TEST_QUIZ_ID = "test_quiz"

    val OPTIONS_SPORT_1 = listOf(Option("New York Bulls"), Option("Los Angeles Kings"), Option("Golden State Warriors"), Option("Houston Rocket"))
    val QUESTIONS_SPORT = listOf(Question("Which one is correct team name in NBA?", OPTIONS_SPORT_1, 4))

    val OPTIONS_MATHS_1 = listOf(Option("10"), Option("11"), Option("12"), Option("13"))
    val OPTIONS_MATHS_2 = listOf(Option("1"), Option("2"), Option("3"), Option("4"))
    val QUESTIONS_MATHS = listOf(Question("5 + 7 = ?", OPTIONS_MATHS_1, 3), Question("12 - 8 = ?", OPTIONS_MATHS_2, 4))

    val TOPICS = listOf(Topic("sport", QUESTIONS_SPORT), Topic("maths", QUESTIONS_MATHS))

    val TEST_QUIZ = Quiz(TEST_QUIZ_ID, TOPICS)
  }

  fun readFile(filePath : String) : String = this::class.java.classLoader.getResource(filePath).readText()

}