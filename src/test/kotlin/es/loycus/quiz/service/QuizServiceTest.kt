package es.loycus.quiz.service

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import es.loycus.quiz.QuizTestWithFongo
import org.json.JSONObject
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class QuizServiceTest : QuizTestWithFongo (initializeTestData = true) {

  @Autowired
  lateinit var quizService : QuizService

  @Test
  fun testFind() {
    val expected = readFile(REQUEST_FILE)

    val quiz = quizService.findById(TEST_QUIZ_ID)
    val actual = jacksonObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true).writeValueAsString(quiz)
    logger.info(actual)

    assertEquals(expected, actual)
  }

  companion object {
    val logger: Logger = LoggerFactory.getLogger(QuizServiceTest::class.java)
  }

}