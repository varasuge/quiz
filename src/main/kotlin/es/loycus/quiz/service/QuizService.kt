package es.loycus.quiz.service

import es.loycus.quiz.exception.NotFoundException
import es.loycus.quiz.model.Option
import es.loycus.quiz.model.Question
import es.loycus.quiz.model.Quiz
import es.loycus.quiz.model.Topic
import es.loycus.quiz.repository.QuizRepository
import es.loycus.quiz.dto.QuestionDto
import es.loycus.quiz.dto.QuizDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

interface QuizService {
  fun findById(id : String) : QuizDto
//  fun findAll(): List<QuizDto>
  fun create(request: QuizDto) : String
  fun update(id: String, request: QuizDto)
  fun deleteById(id: String)
}

@Service("quizService")
class QuizImpl : QuizService {

  @Autowired
  lateinit var quizRepository : QuizRepository

  override fun create(request: QuizDto) : String {
    val quiz = toDomain(request)

    return quizRepository.insert(quiz).id
  }

//  override fun findAll(): List<QuizDto> {
//    val list = quizRepository.findAll()
//  }

  override fun findById(id : String): QuizDto {
    val quiz = quizRepository.findById(id).orElseThrow { NotFoundException("Quiz with id $id not found") }

    val map = quiz.topics.associateBy({ it.topic }, { var i : Int = 1; it.questions.associateBy({ "q".plus(i++) }, { QuestionDto(it.question, it.options.map { it.option }, it.options.get(it.answer - 1).option) } )})
    return QuizDto(map)
  }

  override fun update(id: String, request: QuizDto) {
    val quiz = quizRepository.findById(id).get()

    quizRepository.save(
      quiz.apply {
        topics = toDomain(request).topics
      }
    )
  }

  override fun deleteById(id: String) {
    val quiz = quizRepository.findById(id).get()

    quizRepository.delete(quiz)
  }

  private fun toDomain(request: QuizDto) : Quiz {
    var topics: MutableList<Topic> = mutableListOf()

    for (keyTopic in request.quiz.keys) {
      val topic = Topic(
        topic = keyTopic,
        questions = emptyList()
      )

      var questions: MutableList<Question> = mutableListOf()

      for (keyQuestion in request.quiz[keyTopic]!!.keys) {
        val question = Question(
          question = request.quiz[keyTopic]!![keyQuestion]!!.question,
          options = emptyList(),
          answer = -1
        )

        var options: MutableList<Option> = mutableListOf()
        for (option in request.quiz[keyTopic]!![keyQuestion]!!.options) {
          options.add(Option(option))
        }

        question.options = options
        question.answer = options.indexOf(Option(request.quiz[keyTopic]!![keyQuestion]!!.answer)) + 1

        questions.add(question)
      }

      topic.questions = questions
      topics.add(topic)
    }

    return Quiz(topics = topics)
  }

}