package es.loycus.quiz.repository

import es.loycus.quiz.model.Quiz
import org.springframework.data.mongodb.repository.MongoRepository

interface QuizRepository : MongoRepository<Quiz, String>