package es.loycus.quiz.dto

class QuizDto(
    val quiz : Map<String, Map<String, QuestionDto>>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as QuizDto

        if (quiz != other.quiz) return false

        return true
    }

}