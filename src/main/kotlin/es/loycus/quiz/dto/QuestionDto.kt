package es.loycus.quiz.dto

class QuestionDto(
    val question : String,
    val options : List<String>,
    val answer : String
)