package es.loycus.quiz.controller

import es.loycus.quiz.dto.QuizDto
import es.loycus.quiz.service.QuizService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
class QuizController {

  @Autowired
  lateinit var quizService: QuizService

  @GetMapping("/quiz/{id}")
  fun find(@PathVariable id: String) : QuizDto = quizService.findById(id)

  @PostMapping("/quiz")
  fun save(@RequestBody request : QuizDto) {
    quizService.create(request)
  }

  @PutMapping("/quiz/{id}")
  fun update(@PathVariable id: String, @RequestBody request : QuizDto) {
    quizService.update(id, request)
  }

  @DeleteMapping("/quiz/{id}")
  fun update(@PathVariable id: String) {
    quizService.deleteById(id)
  }

}