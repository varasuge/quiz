package es.loycus.quiz.model

import org.bson.types.ObjectId

data class Quiz(val id : String = ObjectId().toHexString(), var topics : List<Topic>)

data class Topic(val topic : String, var questions : List<Question>)

data class Question(val question : String, var options : List<Option>, var answer: Int)

data class Option(val option : String)